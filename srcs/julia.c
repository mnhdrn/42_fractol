/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 15:33:41 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/05 16:25:19 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <stdio.h>

#define WIN_W WIN1_W
#define WIN_H WIN1_H

static void		reinit(t_data *data)
{
	data->x1 = -1;
	data->x2 = 1;
	data->y1 = -1.2;
	data->y2 = 1.2;
	data->cr = 0;
	data->ci = 0;
	data->zr = 0;
	data->zi = 0;
	data->init = 1;
}

static void		draw(int i, int x, int y, t_data *data)
{
	int			tmp;

	tmp = (i * data->color) / data->imax;
	tmp += data->hue;
	(i != data->imax) ? mlx_pixel_to_img(data, x, y, tmp) : 0;
}

static void		iteration(int x, int y, t_data *data)
{
	int			i;
	double		tmp;

	i = 0;
	tmp = 0;
	data->cr = 0.285;
	data->ci = 0.01;
	data->zr = x / data->zoom + data->x1;
	data->zi = y / data->zoom + data->y1;
	if (data->julia_act == 1)
	{
		data->cr = data->mouse_x / data->zoom + data->x1;
		data->ci = data->mouse_y / data->zoom + data->y1;
	}
	while (data->zr * data->zr + data->zi * data->zi < 4 && i < data->imax)
	{
		tmp = data->zr;
		data->zr = data->zr * data->zr - data->zi * data->zi + data->cr;
		data->zi = 2 * data->zi * tmp + data->ci;
		i++;
	}
	draw(i, x, y, data);
}

void			p_julia(int x, int y, t_data *data)
{
	if (data->init == 0)
		reinit(data);
	while (y < WIN_H)
	{
		x = 0;
		while (x < WIN_W)
		{
			iteration(x, y, data);
			x++;
		}
		y++;
	}
}
