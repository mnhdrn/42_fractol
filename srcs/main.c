/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/03 15:20:32 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/05 17:33:01 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

#define WIN_W data->g_env.win_w
#define WIN_H data->g_env.win_h

static void				m_indication(void)
{
	ft_putendl("Bienvenu  :: use ARROWS to move");
	ft_putendl("--------  :: use '1' '2' to increase or decrease iteration");
	ft_putendl("--------  :: use 'C', to change the color");
	ft_putendl("--------  :: use '0', to reset");
	ft_putendl("--------  :: use 'v', to switch in mouse mode for Julia");
	ft_putendl("--------  :: use '-' '=', to zoom and dezoom");
	ft_putendl("--------  :: use '[' ']', to change the fractal");
	ft_putendl("Thank you :: Please use 'escape' for leaving the programs");
}

static void				init_win(t_data *data)
{
	WIN_W = WIN1_W;
	WIN_H = WIN1_H;
	WIN.off_x = 0;
	WIN.off_y = 0;
	WIN.mlx_ptr = mlx_init();
	if (!WIN.mlx_ptr)
		exit(1);
	WIN.win_ptr = mlx_new_window(WIN.mlx_ptr, WIN_W, WIN_H, (char *)"FRACTOL");
	WIN.img_ptr = NULL;
}

static void				init_struct(t_data *data)
{
	data->init = 0;
	data->fid = 0;
	data->julia_act = 1;
	data->mouse_x = 5;
	data->mouse_y = 5;
	data->x1 = -2.1;
	data->x2 = 1.9;
	data->y1 = -1.2;
	data->y2 = 1.2;
	data->cr = 0;
	data->ci = 0;
	data->zr = 0;
	data->zi = 0;
	data->zoom = (WIN_W / 4) + 10;
	data->imax = 100;
	data->color = 0x200235;
	data->hue = 0;
	IMG.addr = NULL;
}

static void				ft_fractol(int id)
{
	t_data				data;

	m_indication();
	init_win(&data);
	init_struct(&data);
	data.fid = id;
	m_print(data.fid, &data);
	mlx_hook(data.g_env.win_ptr, 2, (1L << 0), m_hook, &data);
	mlx_key_hook(data.g_env.win_ptr, m_hook, &data);
	mlx_hook(data.g_env.win_ptr, 1, (1L << 0), m_rhook, &data);
	mlx_key_hook(data.g_env.win_ptr, m_rhook, &data);
	mlx_mouse_hook(data.g_env.win_ptr, m_mouse, &data);
	mlx_hook(data.g_env.win_ptr, 6, (1L << 6), m_mouse_pos, &data);
	mlx_loop(data.g_env.mlx_ptr);
}

int						main(int ac, char **av)
{
	if (ac != 2)
		ft_putendl("Usage :: ./fractol 'number [0-2]'");
	else
	{
		if (ft_strequ("0", av[1]))
			ft_fractol(0);
		else if (ft_strequ("1", av[1]))
			ft_fractol(1);
		else if (ft_strequ("2", av[1]))
			ft_fractol(2);
		else if (ft_strequ("3", av[1]))
			ft_fractol(3);
		else
			ft_putendl("Wrong arguments, Usage :: ./fractol 'number [0-2]'");
	}
	return (0);
}
