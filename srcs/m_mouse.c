/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_mouse.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/04 01:23:08 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/05 16:36:35 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

#define WIN_W WIN1_W
#define WIN_H WIN1_H

int				m_mouse_pos(int x, int y, t_data *data)
{
	if (data->fid == 1 && data->julia_act == 1)
	{
		if (y > 0 && x > 0 && y < WIN_H && x < WIN_W)
			data->mouse_x = x;
		if (y > 0 && x > 0 && y < WIN_H && x < WIN_W)
			data->mouse_y = y;
		mlx_redraw_img(data);
	}
	return (0);
}

int				m_mouse(int key, int x, int y, t_data *data)
{
	if (key == 1)
		f_zoom(1, x, y, data);
	if (key == 2)
		f_zoom(0, x, y, data);
	if (key == 4)
		f_zoom(0, (WIN_W / 2), (WIN_H / 2), data);
	if (key == 5)
		f_zoom(1, (WIN_W / 2), (WIN_H / 2), data);
	mlx_redraw_img(data);
	return (0);
}
