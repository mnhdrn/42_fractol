/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_rhook.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/05 15:45:33 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/05 16:52:25 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int				m_rhook(int key, t_data *data)
{
	(key == 0x1E) ? f_fractol(0, data) : 0;
	(key == 0x21) ? f_fractol(1, data) : 0;
	(key == 0x09) ? f_julia_act(data) : 0;
	mlx_redraw_img(data);
	return (0);
}
